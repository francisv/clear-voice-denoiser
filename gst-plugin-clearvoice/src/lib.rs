use gst::glib;

mod clearvoice;

fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    clearvoice::register(plugin)?;
    Ok(())
}

gst::plugin_define!(
    clearvoice,
    env!("CARGO_PKG_DESCRIPTION"),
    plugin_init,
    concat!(env!("CARGO_PKG_VERSION"), "-", env!("COMMIT_ID")),
    "MIT/X11",
    env!("CARGO_PKG_NAME"),
    env!("CARGO_PKG_NAME"),
    env!("CARGO_PKG_REPOSITORY"),
    env!("BUILD_REL_DATE")
);
