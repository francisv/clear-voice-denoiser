use gst::glib;
// use gst::prelude::*;
use gst_audio::subclass::prelude::*;

// use std::sync::Mutex;

use once_cell::sync::Lazy;

static _CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "clearvoice",
        gst::DebugColorFlags::empty(),
        Some("Clear Voice Denoiser"),
    )
});

#[derive(Default)]
pub struct ClearVoice {}

impl ClearVoice {}

#[glib::object_subclass]
impl ObjectSubclass for ClearVoice {
    const NAME: &'static str = "GstClearVoice";
    type Type = super::ClearVoice;
    type ParentType = gst_audio::AudioFilter;
}

impl ObjectImpl for ClearVoice {}
impl GstObjectImpl for ClearVoice {}

impl ElementImpl for ClearVoice {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "Clear Voice Denoiser",
                "Filter/Effect/Audio",
                "Improves voice in noisy recording",
                "Francisco Javier Velázquez-García <dr.vg@oss-consulting.as>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }
}

impl BaseTransformImpl for ClearVoice {
    const MODE: gst_base::subclass::BaseTransformMode =
        gst_base::subclass::BaseTransformMode::NeverInPlace;
    const PASSTHROUGH_ON_SAME_CAPS: bool = false;
    const TRANSFORM_IP_ON_PASSTHROUGH: bool = false;
}
impl AudioFilterImpl for ClearVoice {}

