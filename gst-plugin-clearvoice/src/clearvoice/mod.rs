use gst::glib;
use gst::prelude::*;

mod imp;

// The public Rust wrapper type for our element
glib::wrapper! {
    pub struct ClearVoice(ObjectSubclass<imp::ClearVoice>) @extends gst_base::BaseTransform, gst::Element, gst::Object;
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "clearvoice",
        gst::Rank::NONE,
        ClearVoice::static_type(),
    )
}
