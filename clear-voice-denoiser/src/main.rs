extern crate gstreamer as gst;

use gst::prelude::*;
use std::path::Path;
use std::env;

/// This is the main entry point for the program that clarifies a
/// noisy voice recording.
///
/// # Examples
///
/// Command to run the program:
///
/// ```ssh
/// $ cargo run filename
/// ```
fn main() {
    // Collect command line arguments.  Skip the first argument as it
    // is the path to the binary.
    let args: Vec<String> = env::args().skip(1).collect();

    // Check if a filename was provided
    if args.len() < 1 {
        eprintln!("Usage: cargo run <filename>");
        std::process::exit(1);
    }

    // Extract the filename from the arguments
    let filename = &args[0];
    println!("Processing file: {}", filename);

    // Check if the file exists
    if !Path::new(filename).exists() {
        println!("File does not exist: {}", filename);
        return; // Early return if the file does not exist
    }

    // Initialize GStreamer
    gst::init().unwrap();

    let pipeline = gst::parse::launch(&format!("filesrc location={filename} \
                 ! rawaudioparse \
                 use-sink-caps=false \
                 format=pcm \
                 pcm-format=u32le \
                 sample-rate=48000 \
                 num-channels=1 \
                 ! audioconvert \
                 ! audioresample \
                 ! audiocheblimit mode=low-pass cutoff=3400 poles=5 \
                 ! audiocheblimit mode=high-pass cutoff=300 poles=5 \
                 ! audiocheblimit mode=low-pass cutoff=3400 poles=5 \
                 ! audiocheblimit mode=high-pass cutoff=300 poles=5 \
                 ! audiodynamic threshold=0.5 ratio=2 \
                 ! equalizer-10bands \
                 band0=-3.0 \
                 band1=-2.0 \
                 band2=-1.5 \
                 band3=2.0 \
                 band4=3.5 \
                 band5=3.0 \
                 band6=2.5 \
                 band7=-1.0 \
                 band8=-2.5 \
                 band9=-3.0 \
                 ! audioamplify amplification=2.0 \
                 ! autoaudiosink")).unwrap();

    // Start playing
    pipeline
        .set_state(gst::State::Playing)
        .expect("Unable to set the pipeline to the `Playing` state");

    // Wait until error or EOS
    let bus = pipeline.bus().unwrap();
    for msg in bus.iter_timed(gst::ClockTime::NONE) {
        use gst::MessageView;

        match msg.view() {
            MessageView::Eos(..) => break,
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.src().map(|s| s.path_string()),
                    err.error(),
                    err.debug()
                );
                break;
            }
            _ => (),
        }
    }

    // Shutdown pipeline
    pipeline
        .set_state(gst::State::Null)
        .expect("Unable to set the pipeline to the `Null` state");
}
